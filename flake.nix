{
  description = "My Emacs configuration.";
  inputs = {
    emacs-overlay = {
      url = "github:nix-community/emacs-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-utils.url = "github:numtide/flake-utils";
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
  };

  outputs = { emacs-overlay, flake-utils, nixpkgs, self, ... }:
    let inherit (flake-utils.lib) eachDefaultSystem mkApp;
    in eachDefaultSystem (system:
      let
        overlays = [ emacs-overlay.overlay ];
        pkgs = nixpkgs.legacyPackages.${system}.extend
          (nixpkgs.lib.composeManyExtensions overlays);
      in {
        packages.default = pkgs.callPackage ./default.nix {
          inherit (pkgs.python3Packages) jupytext;
        };

        apps.default = mkApp {
          drv = self.packages.${system}.default;
          exePath = "/bin/emacs";
        };

        devShells.default =
          pkgs.mkShell { buildInputs = with pkgs; [ nil nixfmt ]; };
      });
}
