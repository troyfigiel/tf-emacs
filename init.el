(eval-when-compile
  (require 'use-package)
  (setq use-package-verbose t))
(require 'bind-key)
(require 'xdg)

(defvar tf-emacs/directories
  (list :notes (getenv "NOTES_DIR")
	:library (getenv "LIBRARY_DIR")
	:org (getenv "ORG_DIR")
	:data (getenv "EMACS_DATA_DIR")
	:cache (file-name-concat (xdg-cache-home) "emacs")))

;; ;; TODO: Check if I still run into issues with recentf and tramp. If
;; ;; I do, I can set recentf-keep to '(file-remote-p file-readable-p).
;; (use-package recentf
;;   ;; It is important to turn recentf-auto-cleanup to never *before*
;;   ;; turning on recentf-mode. By default recentf will run an
;;   ;; auto-cleanup when turning on recentf-mode.
;;   :init (setq recentf-auto-cleanup 'never)
;;   :config
;;   (recentf-mode 1)
;;   (run-at-time nil 300 'recentf-save-list))

(use-package no-littering
  :ensure
  :custom
  (auto-save-file-name-transforms
   `((".*" ,(no-littering-expand-var-file-name "auto-save/") t))))
;; :config
;; (add-to-list 'recentf-exclude
;;	       (recentf-expand-file-name no-littering-var-directory))
;; (add-to-list 'recentf-exclude
;;	       (recentf-expand-file-name no-littering-etc-directory)))

;; TODO: calc-embedded can be used for (indefinite) integration,
;; factoring polynomials and much more. I should look into some of the
;; details, because it seems amazing.

(use-package emacs
  :hook ((prog-mode . display-line-numbers-mode)
	 (text-mode . (lambda () (setq fill-column 90))))
  :bind (("C-x C-b" . ibuffer)
	 ;; When I try to kill all text up to an uncommon character
	 ;; such as a quote, I have to use the character before the
	 ;; quote which could appear several times. This makes it a
	 ;; bit annoying to use `zap-to-char' as the default.
	 ("M-z" . zap-up-to-char))
  :custom
  (bookmark-default-file
   (file-name-concat (plist-get tf-emacs/directories :data)
		     "bookmarks.el"))
  (custom-file null-device)
  ;; I never use backup files anyway and they can leak secrets if not
  ;; used carefully.
  (make-backup-files nil)
  (initial-scratch-message nil)
  (sentence-end-double-space nil)
  ;; 1000 is a random value. It simply needs to be a large number.
  (scroll-conservatively 1000)
  (scroll-step 1)
  (visible-bell t)
  :config
  (auto-save-visited-mode 1)
  (put 'downcase-region 'disabled nil)
  (put 'upcase-region 'disabled nil)
  (put 'scroll-left 'disabled nil)
  (column-number-mode 1)
  (delete-selection-mode 1)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (set-fringe-mode 15)
  (tool-bar-mode -1)
  (tooltip-mode -1)
  (window-divider-mode 1))

;; Themes

;; TODO: The doom-one theme is not legible to me when I am
;; programming. The modus-themes are very legible, but I prefer the
;; overall fainter colours of doom-one. Have a look at which colours
;; are used and try to copy some of them to the modus-themes.
(use-package emacs
  :custom
  (modus-themes-common-palette-overrides
   '((border-mode-line-active unspecified)
     (border-mode-line-inactive unspecified)
     (comment yellow-faint)
     (string green)))
  :config (load-theme 'modus-vivendi-tinted t))

(use-package doom-modeline
  :ensure
  :custom (doom-modeline-icon nil)
  :config (doom-modeline-mode 1))

;; Eshell

(use-package eshell
  :bind ("C-c E" . eshell)
  :custom
  (inhibit-startup-screen t)
  (initial-buffer-choice #'eshell)
  (eshell-banner-message "\n"))

;; We need to load `em-hist', because `eshell-hist-mode-map' is
;; defined here. Otherwise we will run into an undefined variable
;; error.
(use-package em-hist
  :bind ( :map eshell-hist-mode-map
	  ("M-r" . consult-history)
	  ("M-s" . nil)))

(use-package code-cells
  :ensure
  :hook (python-ts-mode . code-cells-mode)
  :config
  (let ((map code-cells-mode-map))
    (define-key map "n" (code-cells-speed-key 'code-cells-forward-cell))
    (define-key map "p" (code-cells-speed-key 'code-cells-backward-cell))
    (define-key map "e" (code-cells-speed-key 'code-cells-eval))
    (define-key map (kbd "TAB") (code-cells-speed-key 'outline-cycle))))

;; TODO: This does not build for emacs-pgtk.
;; (use-package jupyter
;;   :ensure
;;   :bind
;;   (("C-c k a" . jupyter-repl-associate-buffer)
;;    ("C-c k s" . jupyter-run-server-repl))
;;   :custom (jupyter-repl-echo-eval-p t))

(use-package apheleia
  :ensure
  ;; I have not found a simpler way to ensure latexindent uses a 4 tab
  ;; width by default.
  :hook ((org-mode LaTeX-mode) . (lambda () (setq tab-width 4)))
  :config
  (add-to-list 'apheleia-mode-alist '(emacs-lisp-mode . lisp-indent))
  (apheleia-global-mode 1))

(use-package cdlatex
  :ensure)

(use-package texmathp
  :ensure auctex)

(use-package xref
  :custom
  (xref-search-program 'ripgrep))

(use-package citar
  :ensure
  :custom
  (citar-notes-paths (list (plist-get tf-emacs/directories :notes)))
  (citar-bibliography (list (file-name-concat
			     (plist-get tf-emacs/directories :library)
			     "references.bib")))
  (citar-library-paths (list (plist-get tf-emacs/directories :library)))
  (citar-at-point-function 'embark-act))

(use-package citar-denote
  :ensure
  :custom
  (citar-denote-keyword "°reference")
  (citar-open-always-create-notes t)
  :config (citar-denote-mode 1))

(use-package denote
  :ensure
  :hook (dired-mode . denote-dired-mode)
  :bind
  (("C-c n a" . denote-keywords-add)
   ("C-c n b" . denote-link-find-backlink)
   ("C-c n d" . denote-keywords-remove)
   ("C-c n f" . denote-link-find-file)
   ("C-c n g" . (lambda () (interactive) (consult-grep denote-directory)))
   ("C-c n i" . denote-link-or-create)
   ("C-c n I" . denote-org-dblock-insert-links)
   ("C-c n o" . denote-open-or-create)
   ("C-c n r" . denote-rename-file-using-front-matter))
  :custom
  (denote-known-keywords nil)
  (denote-directory (plist-get tf-emacs/directories :notes)))

(use-package csv-mode
  :ensure
  :hook ((csv-mode . csv-align-mode)
	 (csv-mode . csv-guess-set-separator)))

(use-package dired
  :hook ((dired-mode . dired-hide-details-mode)
	 (dired-mode . dired-omit-mode))
  :bind (:map
	 dired-mode-map
	 ("C-c C-w" . wdired-change-to-wdired-mode))
  :custom
  (dired-hide-details-hide-symlink-targets nil)
  (dired-kill-when-opening-new-dired-buffer t)
  ;; This hides dotfiles by default.
  (dired-omit-files "^\\.")
  (dired-listing-switches
   "-Ahl --group-directories-first --time-style=long-iso"))

(use-package image-dired
  :custom)

(use-package dired-open
  :ensure
  :custom
  (dired-open-extensions
   '(("avi" . "mpv")
     ("mkv" . "mpv")
     ("wmv" . "mpv")
     ("mp4" . "mpv")
     ("mov" . "mpv"))))

(use-package dired-subtree
  :ensure
  :after dired
  ;; TODO: Should I enable some of these bindings in wdired-mode-map?
  :bind ( :map dired-mode-map
	  ("<tab>" . dired-subtree-toggle)
	  ("* s" . dired-subtree-mark-subtree)
	  ("C-M-u" . dired-subtree-up)
	  ("C-M-p" . dired-subtree-previous-sibling)
	  ("C-M-n" . dired-subtree-next-sibling))
  :custom
  (dired-subtree-use-backgrounds nil)
  (dired-subtree-line-prefix "   "))

;; TODO: Currently annoying with the following bug. Turned off for
;; now. https://github.com/Silex/docker.el/issues/218

;; (use-package docker
;;   :ensure
;;   :bind ("C-c d" . docker))

(use-package docker-compose-mode
  :ensure)

(use-package dockerfile-mode
  :ensure)

(use-package eglot
  :hook ((go-ts-mode
	  go-mod-ts-mode
	  python-ts-mode
	  nix-mode) . eglot-ensure)
  :config (add-to-list 'eglot-server-programs '(nix-mode . ("nil"))))

;; TODO: I am not sure what this does exactly. I think this is a
;; which-key replacement. I have to check, e.g. if I run C-c C-h or
;; C-C ? this window pops-up.prefix-help-command = "#'embark-prefix-help-command";
(use-package embark
  :ensure
  :hook (embark-collect-mode . hl-line-mode)
  :bind* (("C-." . embark-act)
	  ("C-h b" . embark-bindings))
  :custom
  (embark-confirm-act-all nil)
  (embark-help-key (kbd "?"))
  (embark-indicators
   '(embark-minimal-indicator
     embark-highlight-indicator
     embark-isearch-highlight-indicator)))

(use-package flymake
  :bind ( :map flymake-mode-map
	  ("M-n" . flymake-goto-next-error)
	  ("M-p" . flymake-goto-prev-error)))

(use-package fontaine
  :ensure
  :custom
  (fontaine-presets
   '((default
      :default-family "Iosevka Comfy Fixed"
      :default-height 110)))
  :config
  (fontaine-set-preset 'default))

(use-package git-modes
  :ensure
  :mode ("\\.dockerignore\\'" . gitignore-mode))

;; TODO: If I press RET in an Embark export buffer, this does not
;; bring me to helpful. How do I override these functions instead?
;; Should probably be done with `helpful-at-point` somehow.
(use-package helpful
  :ensure
  :bind (("C-h C-h" . helpful-at-point)
	 ([remap describe-command] . helpful-command)
	 ([remap describe-function] . helpful-function)
	 ([remap describe-key] . helpful-key)
	 ([remap describe-symbol] . helpful-symbol)
	 ([remap describe-variable] . helpful-variable)))

(use-package vc
  :custom (vc-follow-symlinks t))

(use-package marginalia
  :ensure
  :config (marginalia-mode 1))

(use-package nix-mode
  :ensure
  :mode "\\.nix\\'")

(use-package orderless
  :ensure
  :custom
  (completion-styles '(orderless))
  (completion-category-overrides nil)
  :config
  (setq completion-category-defaults nil))

(use-package markdown-mode
  :ensure)

(use-package org
  :hook (org-mode . org-cdlatex-mode)
  :config
  (add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
  (add-to-list 'org-structure-template-alist '("py" . "src python"))
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("r" . "src R"))
  (add-to-list 'org-structure-template-alist '("sh" . "src shell"))
  (add-to-list 'org-structure-template-alist '("sq" . "src sql"))
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)
     (shell . t)
     (R . t)
     (sql . t)))
  (push '("conf-unix" . conf-unix) org-src-lang-modes)
  (plist-put org-format-latex-options :scale 1.5)
  ;; This ensures apheleia is still used to automatically format code,
  ;; even when using `org-edit-src-code.' A better solution not
  ;; implemented yet
  ;; https://github.com/radian-software/apheleia/issues/61
  (advice-add #'org-edit-src-save :before (lambda () (call-interactively #'apheleia-format-buffer)))
  :custom
  (org-directory (plist-get tf-emacs/directories :org))
  (org-startup-folded t)
  (org-startup-indented t)
  (org-highlight-latex-and-related '(native))
  (org-use-speed-commands t)

  ;; TODO: The commit hash 9a9faea0ab199616f7888227266fb2305d86a663
  ;; broke the latex image preview functionality with imagemagick for
  ;; me. This was a flake update and I am not sure what caused it. It
  ;; might get fixed over time again

  ;; (org-preview-latex-default-process 'imagemagick)

  (org-preview-latex-image-directory
   (file-name-concat (plist-get tf-emacs/directories :cache) "ltximg"))
  (org-latex-packages-alist
   '(("" "tikz" t)
     ("" "tikz-cd" t)))
  (org-id-method 'ts)
  (org-id-ts-format "%Y%m%dT%H%M%S")
  (org-attach-auto-tag "attachment")
  (org-attach-id-dir (file-name-concat
		      (plist-get tf-emacs/directories :org)
		      "data"))
  (org-attach-id-to-path-function-list
   '(org-attach-id-ts-folder-format
     org-attach-id-uuid-folder-format
     org-attach-id-fallback-folder-format)))

(use-package org-contacts
  :ensure
  :custom
  (org-contacts-files
   (list (file-name-concat
	  (plist-get tf-emacs/directories :org)
	  "contacts.org"))))

(use-package org-agenda
  :bind ("C-c a" . org-agenda)
  :custom
  (org-agenda-files
   (list (file-name-concat
	  (plist-get tf-emacs/directories :org)
	  "journal")))
  (org-agenda-span 'month)
  (org-agenda-todo-ignore-schedules 'all)
  (org-deadline-warning-days 28)
  (org-agenda-skip-deadline-prewarning-if-scheduled t)
  (org-log-done 'time))

(use-package org-journal
  :ensure
  :bind ("C-c j" . org-journal-new-date-entry)
  :custom
  ;; Do not carry over todo items by default.
  (org-journal-carryover-items "")
  (org-journal-dir (file-name-concat
		    (plist-get tf-emacs/directories :org)
		    "journal"))
  (org-journal-date-format "%F %a")
  ;; Do not log the time of day by default.
  (org-journal-time-format "")
  ;; Use a yearly journal instead of the default daily.
  (org-journal-file-format "%Y.org")
  (org-journal-file-type 'yearly))

(use-package org-recur
  :ensure
  :hook ((org-mode . org-recur-mode)
	 (org-agenda-mode . org-recur-agenda-mode))
  :bind ( :map org-recur-agenda-mode-map
	  ("d" . org-recur-finish)
	  ("i" . org-recur-schedule-today))
  :custom
  ;; TODO: Think about setting `org-recur-finish-archive' to `t'
  (org-recur-finish-done t))

(use-package pdf-tools
  :ensure
  :mode ("\\.pdf\\'" . pdf-view-mode))

(use-package python
  :bind ( :map python-ts-mode-map
	  ("C-<right>" . python-indent-shift-right)
	  ("C-<left>" . python-indent-shift-left))
  :hook (python-ts-mode . (lambda () (setq tab-width 4)))
  :custom
  (dap-python-debugger 'debugpy)
  (dap-python-executable "python3")
  (python-shell-interpreter "python3")
  (python-shell-completion-native-enable nil)
  :config
  (add-to-list 'major-mode-remap-alist '(python-mode . python-ts-mode)))

(use-package julia-ts-mode
  :ensure
  :mode "\\.jl$")

(use-package julia-repl
  :ensure
  :hook (julia-ts-mode . julia-repl-mode))

;; TODO: This sets agda2-mode for Emacs. Unfortunately, this requires
;; the Agda version to match the version of the agda-mode. I can use
;; direnv to add packages to Agda, but not change its version and
;; still work with the Emacs mode. This is version 2.6.3 as of writing
;; this comment. Hopefully there is a better fix in the future.
(load-file (let ((coding-system-for-read 'utf-8))
	     (shell-command-to-string "agda-mode locate")))

(use-package proof-general
  :ensure
  :custom (proof-splash-enable nil))

(use-package go-ts-mode
  :mode (("\\.go\\'" . go-ts-mode)
	 ("\\.mod\\'" . go-mod-ts-mode)))

(use-package cov
  :ensure
  :hook (python-mode . cov-mode)
  :custom
  (cov-coverage-mode t)
  (cov-fringe-symbol 'filled-rectangle))

(use-package rainbow-delimiters
  :ensure
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package tab-bar
  :custom (tab-bar-show nil)
  :config
  (tab-bar-mode 1)
  (tab-bar-history-mode 1))

(use-package terraform-mode
  :ensure)

(use-package tree-sitter
  :hook (tree-sitter-after-on . tree-sitter-hl-mode)
  :custom (treesit-font-lock-level 4)
  ;; TODO: I do not understand whether I need to call e.g.
  ;; python-ts-mode, turn on global-tree-sitter-mode or both. It works
  ;; for now, but look into this.
  :config (global-tree-sitter-mode 1))

(use-package tree-sitter-langs
  :ensure)

(use-package expand-region
  :ensure
  :bind ("C-+" . er/expand-region))

(use-package visual-fill-column
  :ensure
  :hook ((text-mode . visual-line-mode)
	 (visual-line-mode . visual-fill-column-mode)))

(use-package windmove
  :config
  (windmove-default-keybindings 'meta)
  (windmove-swap-states-default-keybindings '(shift meta)))

(use-package whitespace-cleanup-mode
  :ensure
  :hook ((prog-mode text-mode) . whitespace-cleanup-mode))

(use-package yaml-mode
  :ensure
  :mode ("\\.dvc\\'" "dvc.lock"))

(use-package which-key
  :ensure
  :config (which-key-mode 1))

;; Magit

(use-package magit
  :ensure
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (vc-follow-symlinks t)
  :config
  ;; This works for now, but can be difficult to test. For example,
  ;; some applications will check if Emacs is running in a tty to
  ;; determine whether to output colours or not. Pre-commit might not
  ;; work well in some cases.
  (setq magit-process-finish-apply-ansi-colors t))

(use-package project
  :bind (([remap project-switch-to-buffer] . consult-project-buffer)
	 :map project-prefix-map
	 ("f" . consult-find)
	 ("g" . consult-ripgrep)
	 ("m" . magit-project-status))
  :custom
  (project-switch-commands
   '((project-dired "Dired")
     (consult-find "Find file")
     (consult-ripgrep "Find regexp")
     (magit-project-status "Magit")
     (project-eshell "Eshell"))))

;; TODO: This displays colours in the compilation buffers, but I still
;; do not know how to show colours in the warnings buffer (for example
;; when direnv is blocked).
(use-package compile
  ;; Ensure the compilation buffer can interpret colours correctly.
  :hook (compilation-filter . ansi-color-compilation-filter)
  ;; Do not cut off any compilation output.
  :custom (compilation-max-output-line-length nil))

;; Pass

(use-package pass
  :ensure
  :init
  (unless (getenv "PASSWORD_STORE_DIR")
    (setenv "PASSWORD_STORE_DIR"
	    (file-name-concat (xdg-data-home) "password-store")))
  :bind ("C-c p" . pass))

(use-package auth-source-pass
  :config (auth-source-pass-enable))

(use-package restclient
  :ensure
  :mode ("\\.http\\'" . restclient-mode))

;; Feed

(use-package elfeed
  :ensure
  :custom
  (elfeed-search-filter "@1-week-ago +unread")
  (elfeed-db-directory
   (file-name-concat (plist-get tf-emacs/directories :data)
		     "elfeed")))

(use-package elfeed-org
  :ensure
  :after elfeed
  :custom
  (rmh-elfeed-org-files
   (list (file-name-concat org-directory "feeds.org")))
  :config (elfeed-org))

;; Email

(use-package notmuch
  :ensure
  :custom
  (notmuch-search-oldest-first nil)
  (sendmail-program "msmtp")
  (send-mail-function #'sendmail-send-it))

;; Completion

(use-package vertico
  :ensure
  :custom (vertico-cycle t)
  :config (vertico-mode 1))

(use-package consult
  :ensure
  ;; We need to demand `consult' to ensure we can immediately use
  ;; `consult-completion-in-region'.
  :demand
  :bind
  (([remap goto-line] . consult-goto-line)
   ([remap imenu] . consult-imenu)
   ([remap yank-pop] . consult-yank-pop)
   ("M-g I" . consult-imenu-multi)
   ("M-g o" . consult-outline)
   ("M-s l" . consult-line)
   ("M-s L" . consult-line-multi)
   :map minibuffer-mode-map
   ("M-r" . consult-history)
   ("M-s" . nil)
   :map comint-mode-map
   ("M-r" . consult-history)
   ("M-s" . nil))
  :custom
  (consult-async-min-input 2)
  (xref-show-xrefs-function #'consult-xref)
  (xref-show-definitions-function #'consult-xref)
  :config
  (setq completion-in-region-function 'consult-completion-in-region))

(use-package consult-eglot
  :ensure
  :bind ("M-g e" . consult-eglot-symbols))

(use-package embark-consult
  :ensure
  :hook (embark-collect-mode . consult-preview-at-point-mode))

;; Misc

(use-package tramp
  :config
  (add-to-list 'tramp-remote-path 'tramp-own-remote-path))


(use-package minibuffer
  :bind ( :map completion-in-region-mode-map
	  ("M-n" . minibuffer-next-completion)
	  ("M-p" . minibuffer-previous-completion)
	  :map minibuffer-local-completion-map
	  ("M-n" . minibuffer-next-completion)
	  ("M-p" . minibuffer-previous-completion))
  :custom
  (completion-auto-select nil)
  (completion-show-help nil)
  (completions-format 'one-column)
  (completions-header-format nil)
  (completions-max-height 10))

;; envrc-mode should be initialized in each buffer before any other
;; minor modes. Therefore, envrc-global-mode should be added after
;; other global minor modes, since each prepends itself to mode
;; specific hooks.
(use-package envrc
  :ensure
  :demand
  :bind ( :map envrc-mode-map
	  ("C-c e" . envrc-command-map)
	  :map envrc-command-map
	  ("R" . envrc-reload-all))
  :config (envrc-global-mode))

(use-package realgud
  :ensure)

(use-package geiser-guile
  :ensure)

(use-package guix
  :ensure)
