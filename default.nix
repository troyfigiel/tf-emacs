{ agda, direnv, emacsWithPackagesFromUsePackage, emacs-pgtk, exiftool, git
, git-annex, imagemagick, jupytext, lib, makeWrapper, pass, ripgrep, sqlite
, symlinkJoin, texlive }:

let
  unwrappedEmacs = emacsWithPackagesFromUsePackage {
    config = ./init.el;
    defaultInitFile = true;
    package = emacs-pgtk;
    alwaysEnsure = false;
    extraEmacsPackages = epkgs:
      [
        # TODO: Is this not better placed in the project directories that need it?
        (epkgs.treesit-grammars.with-grammars (grammars:
          with grammars; [
            tree-sitter-go
            tree-sitter-gomod
            tree-sitter-julia
            tree-sitter-nix
            tree-sitter-python
          ]))
      ];
  };

  # TODO: LSP packages should be moved away from here and preferably moved into my templates. This
  # is where I can use dir-locals and add the LSP packages to direnv.
  binPackages = [
    # Agda needs to be available in my PATH upon startup of Emacs for the agda-mode binary to work.
    # https://monospacedmonologues.com/2021/05/running-agda-with-nix/
    # See also the note in my init file.
    agda
    direnv
    exiftool
    git
    git-annex
    imagemagick
    jupytext
    (pass.withExtensions (ext: with ext; [ pass-otp ]))
    ripgrep
    sqlite
    texlive.combined.scheme-full
  ];

  # TODO: How do I add fonts? How do I add a desktop icon? I can use substituteInPlace somewhere? I
  # should have a look at pkgs/applications/terminal-emulators/xterm/default.nix to see an example
  # of how this could be done.
in symlinkJoin {
  name = "tf-emacs";
  paths = [ unwrappedEmacs ];
  nativeBuildInputs = [ makeWrapper ];
  # I cannot put the binPackages under paths, because this will cause Nix to add the binaries under
  # a different store-path, which causes collisions on NixOS if these packages are also defined in
  # a different environment and linked to my PATH.
  postBuild = ''
    wrapProgram $out/bin/emacs --prefix PATH : "${lib.makeBinPath binPackages}"
  '';
}
